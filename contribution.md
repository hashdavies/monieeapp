# Code Contribution GUIDELINES

#### Purpose of defining these guidelines is to make your that every developer must consider this before reviewing any PR and can refer to these guidelines.

1. Directory name should be `small-case`
2. File names should be as follows `small.js` or `small-case.js`
3. Component name should be `MyComponent`
4. Always use named exports
5. For each component/screen we must have the following structure

   1. `/dir`
      1. `index.js`
      2. `styled.js`
      3. `my-component.js`

6. Putting imports in an order.

   - React import
   - Library imports (Alphabetical order)
   - Absolute imports from the project (Alphabetical order) for 1st, 2nd & above parent level imports.
   - Relative imports (Alphabetical order) from same directory.
   - Import \* as
   - Import `./<some file>.<some extension>`
   - Import Props
   - Import Styles

   Each kind should be separated by an empty line. This makes your imports clean and easy to understand for all the components, 3rd-party libraries, and etc.

7. Remove deprecated & unnecessary commented code.
8. Remove consoles.
9. Shouldn't pass inline functions in JSX.
10. Props and Styles should be in a separate file
11. Avoid Inline Stylings
12. Syntax & Formatting is correct (Linting should help with this)
    - [Google javascript formatting style guide](https://google.github.io/styleguide/jsguide.html#formatting)
13. Should be follow proper coding guidlines.
    - Should use self explanatory names of classes, functions and variables.
    - Should follow [Airbnb Javascript Style guide](https://github.com/airbnb/javascript)
14. Remove unnecessary configration file changes (gitlab, podlock etc)
15. Should add proper documentation/comments for helper/tricky functions.

    - Comments are clear and useful, and mostly explain why instead of what
    - Place the comment on a separate line, not at the end of a line of code.
    - Begin comment text with an uppercase letter.
    - End comment text with a period.
    - Insert one space between the comment delimiter (//) and the comment text.
    - Attach comments to code only where necessary
    - please see the [Google JSDocs style guide](https://google.github.io/styleguide/jsguide.html#jsdoc) for comments.

16. Should remove repeated code and follow the DRY principle.
17. If the file/component is too large then it should be sperated into mutiple files/components.
18. Should follow the style guides for styled files..
19. Every PR must have proper description, scope and screenshots/screencast attached to it.
20. Redux structure should follow our [entities/features](https://gitlab.com/jobcase/confidential-product/member-react-native-application/-/blob/develop/docs/REDUX.md) pattern.
