export const ANIMATIONS = {
  onBoardingScreen1: require('./onboarding-lottie-1.json'),
  onBoardingScreen2: require('./onboarding-lottie-2.json'),
  onBoardingScreen3: require('./onboarding-lottie-3.json'),
  onBoardingScreen4: require('./onboarding-lottie-4.json'),
  loader: require('./loader.json'),
  dashboardBg: require('./dashboard-bg.json'),
  homeIcon: require('./home-icon.json'),
  connectIcon: require('./connect-icon.json'),
  historyIcon: require('./history-icon.json'),
  moreIcon: require('./more-icon.json'),
  splashScreen: require('./splash-screen.json'),
};
