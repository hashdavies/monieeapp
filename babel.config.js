module.exports = {
  presets: ['module:metro-react-native-babel-preset'],
  plugins: [
    [
      'module-resolver',
      {
        root: ['.'],
        extensions: ['.ios.js', '.android.js', '.js', '.ts', '.tsx', '.json'],
        alias: {
          '@assets': './assets',
          '@api': './src/api',
          '@screens': './src/screens',
          '@navigation': './src/navigation',
          '@contexts': './src/contexts',
          '@components': './src/components',
          '@constants': './src/constants',
          '@utils': './src/utils',
        },
      },
    ],
    'react-native-reanimated/plugin',
  ],
};
