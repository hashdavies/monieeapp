// Core Packages
import React, { useEffect, useState } from 'react';
import SplashScreen from 'react-native-splash-screen';
import { NavigationContainer } from '@react-navigation/native';
import { QueryClient, QueryClientProvider } from 'react-query';
import 'react-native-gesture-handler';

// Custom Components
import { Loader } from '@components/loader';

// Utils
import { ROUTE_NAMES } from '@navigation/route-names';
import { FONTS } from '@utils/theme/fonts';
import { useFonts } from 'expo-font';
import { MainStack } from './src/navigation/navigation';
import { StatusBar } from 'react-native';

const queryClient = new QueryClient();

const App = () => {
  useEffect(() => SplashScreen.hide(), []);
  const [isReady] = useFonts({ ...FONTS });

  if (!isReady) {
    return null;
  }

  return (
    <>
      <QueryClientProvider client={queryClient}>
        <StatusBar barStyle="light-content" />

        <NavigationContainer fallback={<Loader />}>
          <MainStack />
        </NavigationContainer>
      </QueryClientProvider>
    </>
  );
};

export default App;
