export const SPACING = {
  tiny: 5,
  small: 10,
  default: 15,
  large: 20,
  xLarge: 25,
  huge: 30,
};
