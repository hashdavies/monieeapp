import { IMAGES } from '@utils/images';
import { COLORS } from '@utils/theme';
import { ROUTE_NAMES } from '@navigation/route-names';

export const PHONE_REGEX = /^[0-9\b]+$/;
export const NUMBER_REGEX = /^[0-9]*$/;
export const ALPHA_REGEX = /^[A-Za-z ]+$/;
export const EXPIRY_REGEX = /^(0[1-9]|1[0-2])\/?([0-9]{4}|[0-9]{2})$/;
export const CARD_NUMBER_REGEX =
  /^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$/;
export const EMAIL_REGEX =
  /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const UserContactList = [
  {
    ImageSource: IMAGES.Img1,
    contactName: 'Taye James',
    miniDetail: '+44 8164431677',
    activate: true,
  },
  {
    ImageSource: IMAGES.Img1,
    contactName: 'Taye James',
    miniDetail: '+44 8164431677',
    activate: true,
  },
  {
    ImageSource: IMAGES.Img1,
    contactName: 'Jinad David',
    miniDetail: '+44 8164431677',
    activate: true,
  },
  {
    ImageSource: IMAGES.Img1,
    contactName: 'Tobiloba Kayode',
    miniDetail: '+44 8164431677',
    activate: true,
  },
];
