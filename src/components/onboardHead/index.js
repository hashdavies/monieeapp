// Core Packages
import React from 'react';

// Styles
import {
  OnBoardContainer,
  GroupView,
  ActionWrap,
  Title,
  SubTitle,
  Image,
} from './styles';

export const OnBoardHead = ({
  title,
  subTitle,
  imageLink,
  handleOnPress,
  style,
}) => {
  return (
    <OnBoardContainer style={style}>
      <GroupView>
        {imageLink && (
          <ActionWrap onPress={handleOnPress}>
            <Image source={imageLink} />
          </ActionWrap>
        )}
      </GroupView>
      <Title>{title}</Title>
      <SubTitle>{subTitle}</SubTitle>
    </OnBoardContainer>
  );
};
