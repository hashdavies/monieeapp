// Core Packages
import React from 'react';

// Custom Component
import Button from '@components/button';

// Utils
import {
  Wrapper,
  FLexTabTopImage,
  FlexCenter,
  WalletBalLabel,
  WalletBalValue,
} from './styles';

export const Card = ({
  source,
  title,
  subTitle,
  subTitle2,
  btnLabel,
  onPress,
}) => {
  return (
    <Wrapper>
      <FlexCenter>
        <FLexTabTopImage source={source} />
        <WalletBalValue>{title}</WalletBalValue>
        <WalletBalLabel>{subTitle}</WalletBalLabel>
        <WalletBalLabel>{subTitle2}</WalletBalLabel>
      </FlexCenter>
      <Button label={btnLabel} disable onPress={onPress} />
    </Wrapper>
  );
};
