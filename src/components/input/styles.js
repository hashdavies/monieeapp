// Core Packages
import styled from 'styled-components/native';

// Utils
import { COLORS, FONT_FAMILY } from '@utils/theme';

export const LabelContainer = styled.View`
  color: ${COLORS.accountNumber};
  padding-bottom: 8px;
`;
export const Label = styled.Text`
  font-family: ${FONT_FAMILY.Gilmer600};
  font-size: 14px;
  color: ${COLORS.accountNumber};
`;
export const InputWrapper = styled.View`
  position: relative;
`;
export const LeftAdornment = styled.View`
  position: absolute;
  z-index: 10;
  top: 0;
  left: 0;
  margin-left: 16px;
  margin-bottom: ${({ hasError }) => (hasError ? '20px' : 0)};
  height: 100%;
  display: flex;
  justify-content: center;
`;
export const RightAdornment = styled.View`
  position: absolute;
  z-index: 10;
  top: 0;
  right: 0;
  margin-right: 16px;
  margin-bottom: ${({ hasError }) => (hasError ? '20px' : 0)};
  height: 100%;
  display: flex;
  justify-content: center;
`;
export const InputField = styled.TextInput`
  width: 100%;
  padding-horizontal: 8px;
  padding-vertical: 16px;
  border-width: 1px;
  border-radius: 10px;
  font-size: ${({ value }) => (value ? '14px' : '12px')};
  font-family: ${({ value }) =>
    value ? FONT_FAMILY.Gilmer500 : FONT_FAMILY.Gilmer400};
`;
export const ErrorText = styled.Text`
  margin-top: 4px;
  margin-left: 12px;
  font-size: 12px;
  color: ${COLORS.alerzoRed};
  font-family: ${FONT_FAMILY.Gilmer400};
`;
