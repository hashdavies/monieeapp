// Core Packages
import React, { useEffect, useRef, useState } from 'react';
import { useFocusEffect } from '@react-navigation/native';

// Custom Components
import {
  HomeAltIcon,
  ConnectAltIcon,
  HistoryAltIcon,
  ActiveTabIndicatorIcon,
  MoreAltIcon,
} from '@assets/svg';

// Utils
import { ANIMATIONS } from '@assets/animations';

// Styles
import { Container, ActiveLabel, InactiveLabel, Lottie } from './styles';

const getLottieSource = tabName => {
  switch (tabName) {
    case 'Home':
      return ANIMATIONS.homeIcon;
    case 'Connect':
      return ANIMATIONS.connectIcon;
    case 'History':
      return ANIMATIONS.historyIcon;
    case 'More':
      return ANIMATIONS.moreIcon;
    default:
      return ANIMATIONS.homeIcon;
  }
};

export const TabIcon = ({ tabName = ' ', isFocused }) => {
  const lottieSource = getLottieSource(tabName);
  const ref = useRef();
  useFocusEffect(() => ref.current?.play());

  if (isFocused) {
    return (
      <Container>
        <Lottie source={lottieSource} autoPlay={false} loop={false} ref={ref} />
        <ActiveLabel>{tabName}</ActiveLabel>
        <ActiveTabIndicatorIcon />
      </Container>
    );
  }
  return (
    <Container>
      {tabName === 'Home' ? <HomeAltIcon /> : null}
      {tabName === 'Connect' ? <ConnectAltIcon /> : null}
      {tabName === 'History' ? <HistoryAltIcon /> : null}
      {tabName === 'More' ? <MoreAltIcon /> : null}
      <InactiveLabel>{tabName}</InactiveLabel>
    </Container>
  );
};
