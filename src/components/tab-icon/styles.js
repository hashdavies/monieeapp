// Core Packages
import styled from 'styled-components/native';
import LottieView from 'lottie-react-native';

// Utils
import { COLORS } from '@utils/theme';

export const Container = styled.View`
  width: 82px;
  flex: 1;
  justify-content: center;
  align-items: center;
`;
export const ActiveLabel = styled.Text`
  font-family: GilmerMedium;
  color: ${COLORS.alerzoBlue};
  font-size: 10px;
  line-height: 12px;
  margin-bottom: 6px;
  margin-top: 4px;
`;
export const InactiveLabel = styled.Text`
  font-family: GilmerMedium;
  color: ${COLORS.accountNumber};
  font-size: 10px;
  line-height: 12px;
  margin-bottom: 6px;
  margin-top: 4px;
`;

export const Lottie = styled(LottieView)`
  width: 24px;
  height: 24px;
`;
