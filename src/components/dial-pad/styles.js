import styled from 'styled-components/native';

export const Container = styled.View`
  justify-content: center;
  padding-left: 4px;
  padding-right: 4px;
`;

export const Row = styled.View`
  align-items: center;
  flex-direction: row;
  justify-content: space-between;
`;

export const Icon = styled.TouchableOpacity``;
