// Core Packages
import React from 'react';
import { DialButton } from './dial-button';

// Custom Components
import { FaceIdIcon, TouchIdIcon, DeleteIcon } from '@assets/svg';

//utils
import { Container, Row, Icon } from './styles';

export const DialPad = ({
  onPress,
  onDeletePress,
  biometricType,
  biometricSignIn,
  useDot,
  showBiometric = true,
}) => {
  const getActionButtons = () => {
    const actionButtons = [];
    if (showBiometric) {
      actionButtons.push({
        icon: biometricType === 'FaceID' ? <FaceIdIcon /> : <TouchIdIcon />,
        onPress: () => biometricSignIn(),
      });
    } else {
      useDot !== undefined ? actionButtons.push('.') : actionButtons.push('c');
    }
    actionButtons.push(0);
    actionButtons.push({
      icon: <DeleteIcon />,
      onPress: () => onDeletePress(),
    });
    return actionButtons;
  };

  const DIAL_PAD = [[1, 2, 3], [4, 5, 6], [7, 8, 9], getActionButtons()];

  return (
    <Container>
      {DIAL_PAD.map((numbers, index) => (
        <Row key={index}>
          {numbers.map((digit, innerIndex) => {
            if (typeof digit === 'object') {
              return (
                <Icon key={innerIndex} onPress={digit.onPress}>
                  {digit.icon}
                </Icon>
              );
            }
            return (
              <DialButton title={digit} onPress={onPress} key={innerIndex} />
            );
          })}
        </Row>
      ))}
    </Container>
  );
};
