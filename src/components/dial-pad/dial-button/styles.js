import styled from 'styled-components/native';

const Wrapper = styled.TouchableOpacity`
  // background-color: #f0f2f5;
  justify-content: center;
  align-items: center;
  height: 70px;
  width: 70px;
  border-radius: 35px;
  margin: 16px;
`;

const TextWrapper = styled.Text`
  font-family: GilmerMedium;
  font-size: 24px;
  line-height: 29px;
  color: #374b58;
`;

export { Wrapper, TextWrapper };
