import React from 'react';

// Utils
import { Wrapper, TextWrapper } from './styles';

export const DialButton = props => (
  <Wrapper onPress={() => props.onPress(props.title)}>
    <TextWrapper variant={props.variant}>{props.title}</TextWrapper>
  </Wrapper>
);
