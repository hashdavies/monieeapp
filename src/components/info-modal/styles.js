// Core Packages
import styled from 'styled-components/native';

// Custom Components
import { Modal } from '../modal';
import Button from '../button';

// Utils
import { COLORS } from '@utils/theme';

export const StyledModal = styled(Modal)`
  padding-bottom: 54px;
  padding-top: 30px;
  padding-horizontal: 16px;
  align-items: center;
`;
export const Title = styled.Text`
  font-family: 'GilmerBold';
  font-size: 14px;
  line-height: 19px;
  text-align: center;
  width: 100%;
  margin-bottom: 10px;
`;
export const Description = styled.Text`
  font-family: 'GilmerMedium';
  font-size: 18px;
  line-height: 24px;
  text-align: center;
  max-width: 90%;
  color: ${COLORS.fadeGrey};
  margin-bottom: 30px;
`;
export const Image = styled.Image`
  height: 238px;
  width: 238px;
  margin-top: 8px;
`;
export const PrimaryButton = styled(Button)`
  margin-top: 4px;
`;
export const SecondaryButton = styled(Button)`
  margin-top: 24px;
`;
