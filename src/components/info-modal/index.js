// Core Packages
import React from 'react';

// Styles
import {
  StyledModal,
  Title,
  Description,
  Image,
  PrimaryButton,
  SecondaryButton,
} from './styles';

export const InfoModal = ({
  onSubmit,
  isVisible,
  title,
  description,
  imageSource,
  buttonLabel,
  showSecondaryButton = false,
  secondaryButtonLabel,
  onPressSecondaryButton,
}) => (
  <StyledModal
    isVisible={isVisible}
    alignment="center"
    dismissible={false}
    swipeEnabled={false}
  >
    <Image source={imageSource} />
    {title ? <Title>{title}</Title> : null}
    <Description>{description}</Description>
    <PrimaryButton label={buttonLabel} onPress={onSubmit} />
    {showSecondaryButton ? (
      <SecondaryButton
        variant="outlined"
        label={secondaryButtonLabel}
        onPress={onPressSecondaryButton}
      />
    ) : null}
  </StyledModal>
);
