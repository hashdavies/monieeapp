import styled from 'styled-components/native';
import { COLORS } from '@utils/theme';

const View = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  margin-top: 16px
  margin-bottom: 16px

`;
const LoaderImage = styled.Image`
  width: 24px;
  height: 24px;
`;
const Label = styled.Text`
  font-family: 'GilmerBold';
  font-style: normal;
  font-weight: 600;
  font-size: 12px;
  line-height: 14px;
  color: #7890b5;
  color: ${COLORS.alerzoBlueTwo};
  margin-left: 3px;
`;

export { View, LoaderImage, Label };
