import React from 'react';
import { View, LoaderImage, Label } from './styles';

export const AccountLoader = props => {
  return (
    <View>
      <LoaderImage source={props.loaderImage} />
      <Label>{props.LoaderLabel}</Label>
    </View>
  );
};
