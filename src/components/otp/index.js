import React, { useEffect, useState } from 'react';
import { PinInput } from '@components/pin-input/plain-pin-input';
import { DialPad } from '@components/dial-pad';
import { ROUTE_NAMES } from '@navigation/route-names';
import { OnBoardHead } from '@components/onboardHead';
import { IMAGES } from '@utils/images';

import { Container, StyledPinInput, ForgetPassword } from './styled';

const PIN_SIZE = 4;

export const Otp = ({
  navigation,
  title,
  subTitle,
  nextScreen,
  routeTo,
  style,
  showBackNavigation,
  forgetPasswordText,
}) => {
  const [pin, setPin] = useState('');
  useEffect(() => {
    if (pin.length === PIN_SIZE) {
      navigation.navigate(routeTo);
    }
  }, [navigation, pin]);
  const handleInput = digit => {
    setPin(pin + digit);
  };
  return (
    <Container>
      {showBackNavigation === undefined ? (
        <OnBoardHead
          style={style}
          imageLink={IMAGES.ARROW_LEFT}
          title={title}
          subTitle={subTitle}
        />
      ) : (
        <OnBoardHead style={style} title={title} subTitle={subTitle} />
      )}

      <StyledPinInput>
        <PinInput pin={pin} size={PIN_SIZE} />
      </StyledPinInput>
      {forgetPasswordText && (
        <ForgetPassword>{forgetPasswordText}</ForgetPassword>
      )}
      <DialPad
        onPress={digit => handleInput(digit)}
        onDeletePress={() => pin && setPin(pin.slice(0, -1))}
        showBiometric={false}
      />
    </Container>
  );
};
