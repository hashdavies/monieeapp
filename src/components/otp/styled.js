import styled from 'styled-components/native';
import { COLORS } from '@utils/theme';

export const Container = styled.ScrollView`
  flex-direction: column;
  width: 100%;
  background: ${COLORS.white};
  padding-top: 67px;
  padding-left: 20px;
  padding-right: 20px;
  flex: 1;
`;
export const ForgetPassword = styled.Text`
  font-family: 'ABeeZee';
  font-style: italic;
  font-weight: 400;
  font-size: 14px;
  line-height: 14px;
  text-align: center;
  color: #333333;
  margin-top: 25px;
  margin-bottom: 25px;
`;

export const StyledPinInput = styled.View`
  margin-top: 40px;
  margin-bottom: 40px;
`;
