import styled from 'styled-components/native';

const Container = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
`;

const Wrapper = styled.View`
  padding: 16px;
  background: white;
  border-radius: 8px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  margin-top: 8px;
  margin-bottom: 8px;
`;

const Title = styled.Text`
  font-family: 'GilmerMedium';
  font-size: 14px;
  line-height: 17px;
  color: #5976a1;
`;

const SubTitle = styled.Text`
  font-family: 'GilmerRegular';
  font-size: 10px;
  line-height: 12px;
  display: flex;
  align-items: center;
  padding-top: 8px;
  /* grey 2 */

  color: #a5b0b7;
`;

const ActionButton = styled.Text`
  font-family: 'GilmerBold';
  font-size: 10px;
  line-height: 12px;
  /* identical to box height */
  padding-top: 8px;

  display: flex;
  align-items: center;

  /* P2 Blue */

  color: #5976a1;
`;

export { Container, Wrapper, Title, SubTitle, ActionButton };
