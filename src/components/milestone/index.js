import React from 'react';
import { Image, View } from 'react-native';
// Utils
import { Container, Wrapper, Title, SubTitle, ActionButton } from './styles';
import { Images } from '@utils/images';

export const Milestone = () => (
  <Container>
    <Image source={require('@assets/images/dot.png')} />
    <Wrapper>
      <View>
        <Title>Set secret question</Title>
        <SubTitle>
          Create a secret question to for extra security layer{' '}
        </SubTitle>
        <ActionButton>Create Now </ActionButton>
      </View>
      <View>
        <Image source={Images.FUND_ACCOUNT.ICON} />
      </View>
    </Wrapper>
  </Container>
);
