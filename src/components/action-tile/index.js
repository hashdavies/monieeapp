import React from 'react';
// import { Image } from 'react-native';
import { Wrapper, Icon, Image, ImageBackground, View, Title } from './styles';

export const ActionTile = ({
  backgroundColor,
  backgroundImage,
  icon,
  title,
  title2,
  icon2,
  action,
  isLocked,
}) => (
  <Wrapper backgroundColor={backgroundColor} onPress={action}>
    <ImageBackground source={backgroundImage} resizeMode="cover">
      {isLocked ? <Image source={icon2} /> : null}
      <Icon source={icon} />
      <View>
        <Title>{title}</Title>
        <Title>{title2}</Title>
      </View>
    </ImageBackground>
  </Wrapper>
);
