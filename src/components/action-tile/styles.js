import styled from 'styled-components/native';

const Wrapper = styled.TouchableOpacity`
  height: 101px;
  width: 30%;
  background-color: ${({ backgroundColor }) => backgroundColor};
  border-radius: 10px;
  margin-bottom: 16px;
  overflow: hidden;
`;

const Icon = styled.Image`
  width: 36px;
  height: 36px;
`;
const Image = styled.Image`
  position: absolute;
  right: 10px;
  top: 10px;
`;
const ImageBackground = styled.ImageBackground`
  display: flex;
  padding: 10px;
  height: 101px;
  justify-content: space-between;
`;
const View = styled.View`
  display: flex;
`;

const Title = styled.Text`
  font-family: 'GilmerBold';
  font-size: 14px;
  line-height: 17px;
  color: #475e80;
`;

export { Wrapper, Icon, Image, ImageBackground, View, Title };
