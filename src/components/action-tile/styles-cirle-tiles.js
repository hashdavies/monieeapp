import styled from 'styled-components/native';

const Wrapper = styled.TouchableOpacity`
  height: 58px;
  width: 58px;
  background-color: ${({ backgroundColor }) => backgroundColor};
  border-radius: 29px;
  margin-right: 16px;
  margin-left: ${({ marginLeft }) => marginLeft};
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

const Icon = styled.Image`
  width: 30px;
  height: 30px;
`;

const Title = styled.Text`
  font-family: 'GilmerRegular';
  font-size: 10px;
  line-height: 12px;
  color: #5976a1;
`;

export { Wrapper, Icon, Title };
