import React from 'react';
//utils
import { Wrapper, Icon, Title } from './styles-cirle-tiles';

export const CircleActionTile = ({
  backgroundColor,
  icon,
  title,
  marginLeft = 0,
}) => (
  <Wrapper backgroundColor={backgroundColor} marginLeft={marginLeft}>
    <Icon source={icon} />
    <Title>{title}</Title>
  </Wrapper>
);
