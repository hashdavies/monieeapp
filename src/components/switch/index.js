import React, { useState } from 'react';
import { Switch } from 'react-native';
import { View, Label } from './styles';

export const SwitchToggle = props => {
  const [isFocused, setIsFocused] = useState(false);
  return (
    <View>
      <Switch
        onValueChange={props.onValueChange}
        trackColor={props.thumbColor}
        thumbColor={props.trackColor}
        value={props.value}
      />
      <Label>{props.switchLabel}</Label>
    </View>
  );
};
