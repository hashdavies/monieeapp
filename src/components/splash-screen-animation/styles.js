// Core Packages
import styled from 'styled-components/native';
import LottieView from 'lottie-react-native';

// Utils
import { COLORS } from '@utils/theme';

export const Wrapper = styled.View`
  position: absolute;
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
  margin: 0;
  z-index: 9999;
  background-color: ${COLORS.alerzoBlue};
  width: ${({ width }) => width + 'px'};
  height: ${({ height }) => height + 'px'};
`;
export const Lottie = styled(LottieView)`
  width: 100%;
`;
