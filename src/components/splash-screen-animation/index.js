// Core Packages
import React from 'react';
import { useWindowDimensions } from 'react-native';

// Utils
import { ANIMATIONS } from '@assets/animations';

// Styles
import { Wrapper, Lottie } from './styles';

export const SplashScreenAnimation = ({ onDone }) => {
  const { width, height } = useWindowDimensions();
  return (
    <Wrapper width={width} height={height}>
      <Lottie
        source={ANIMATIONS.splashScreen}
        autoPlay
        loop={false}
        resizeMode="cover"
        onAnimationFinish={onDone}
      />
    </Wrapper>
  );
};
