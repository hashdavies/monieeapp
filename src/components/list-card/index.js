import React from 'react';
import { IMAGES } from '@utils/images';

// Utils
import {
  ListWrap,
  ListPlaceholder,
  Image,
  TextView,
  CaretIcone,
  WrapView,
} from './styles';

export const ListCard = props => {
  return (
    <ListWrap onPress={props.onPress}>
      <WrapView>
        <Image source={props.ImageSource} />
        <TextView>
          <ListPlaceholder>{props.listPlaceholder}</ListPlaceholder>
        </TextView>
      </WrapView>
      <CaretIcone source={IMAGES.ArrowRight} />
    </ListWrap>
  );
};
