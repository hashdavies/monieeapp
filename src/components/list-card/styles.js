import styled from 'styled-components/native';
import { COLORS } from '@utils/theme';

const ListWrap = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  width: 100%;
  justify-content: space-between;
  align-items: center;
  border-bottom-width: 1px;
  border-bottom-color: rgba(193, 202, 207, 0.25);
  padding-bottom: 10px;
  padding-top: 10px;
`;
const WrapView = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  width: 80%;
  align-items: center;
`;
const ListPlaceholder = styled.Text`
  font-family: 'GilmerBold';
  font-size: 14px;
  line-height: 14px;
  color: ${COLORS.fadeGrey2};
`;
const Image = styled.Image`
  width: 40px;
  height: 40px;
`;
const TextView = styled.View`
  flex: 1;
  padding-left: 10px;
`;
const CaretIcone = styled.Image`
  width: 7px;
  height: 12px;
`;

export { ListWrap, ListPlaceholder, Image, TextView, CaretIcone, WrapView };
