// Core Packages
import { Text } from 'react-native';
import styled from 'styled-components/native';

export const H1 = styled.Text`
  font-family: GilmerMedium;
  font-size: 32px;
  color: ${({ color }) => color || '#374b58'};
`;
export const H2 = styled.Text`
  font-family: GilmerMedium;
  font-size: 28px;
  color: ${({ color }) => color || '#374b58'};
`;
export const H3 = styled.Text`
  font-family: GilmerMedium;
  font-size: 24px;
  color: ${({ color }) => color || '#374b58'};
`;
export const H4 = styled.Text`
  font-family: GilmerMedium;
  font-size: 20px;
  color: ${({ color }) => color || '#374b58'};
`;
export const H5 = styled.Text`
  font-family: GilmerBold;
  font-size: 18px;
  color: ${({ color }) => color || '#374b58'};
`;
export const H6 = styled.Text`
  font-family: GilmerMedium;
  font-size: 16px;
  color: ${({ color }) => color || '#374b58'};
`;
export const P1 = styled.Text`
  font-family: GilmerMedium;
  font-size: 14px;
  color: ${({ color }) => color || '#374b58'};
`;
export const P2 = styled.Text`
  font-family: GilmerMedium;
  font-size: 12px;
  color: ${({ color }) => color || '#374b58'};
`;
