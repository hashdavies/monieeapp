import styled from 'styled-components/native';
import { COLORS } from '@utils/theme';

const getIsActive = (current, pinSize) => {
  if (current < pinSize) {
    return COLORS.btnDark;
  }
  return COLORS.fadeBlack;
};

const Container = styled.View`
  flex-direction: row;
  justify-content: center;
`;
const InnerContainer = styled.View`
  flex-direction: row;
  justify-content: center;
  background: #fafafa;
  border-radius: 16px;
  padding-left: 10px
  padding-right: 10px
`;

const CircleWrapper = styled.View`
  background: transparent;
  border: 0px solid #e8ebee;
  border-radius: 10px;
  padding: 16px;
  margin-right: 8px;
`;

const Circle = styled.View`
  width: 12px;
  height: 12px;
  border-radius: 7px;
  background-color: ${({ currentIndex, pinSize }) =>
    getIsActive(currentIndex, pinSize)};
`;

export { Container, CircleWrapper, Circle, InnerContainer };
