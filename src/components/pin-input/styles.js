import styled from 'styled-components/native';
import { COLORS } from '@utils/theme';

const getIsActive = (current, pinSize) => {
  if (current < pinSize) {
    return COLORS.alerzoBlue;
  }
  return COLORS.grey;
};

const Container = styled.View`
  flex-direction: row;
  justify-content: center;
`;

const CircleWrapper = styled.View`
  background: white;
  border: 0.8px solid #e8ebee;
  border-radius: 10px;
  padding: 16px;
  margin-right: 8px;
`;

const Circle = styled.View`
  width: 12px;
  height: 12px;
  border-radius: 7px;
  background-color: ${({ currentIndex, pinSize }) =>
    getIsActive(currentIndex, pinSize)};
`;

export { Container, CircleWrapper, Circle };
