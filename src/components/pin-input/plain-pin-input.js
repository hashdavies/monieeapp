import React from 'react';
import { COLORS } from '@utils/theme';

// Utils
import {
  Container,
  CircleWrapper,
  Circle,
  InnerContainer,
} from './plain-styles';

const getIsActive = (current, pinSize) => {
  if (current < pinSize) {
    return COLORS.alerzoBlue;
  }
  return COLORS.grey;
};

export const PinInput = ({ size, pin }) => (
  <Container>
    <InnerContainer>
      {[...Array(size).keys()].map((val, index) => (
        <CircleWrapper key={`pin-${index}`}>
          <Circle pinSize={pin?.length} currentIndex={val} />
        </CircleWrapper>
      ))}
    </InnerContainer>
  </Container>
);
