// Core Packages
import React from 'react';
import styled from 'styled-components/native';
import { useWindowDimensions } from 'react-native';
import LottieView from 'lottie-react-native';

// Utils
import { ANIMATIONS } from '@assets/animations';
import { COLORS } from '@utils/theme';

const Wrapper = styled.View`
  background-color: ${COLORS.whiteOverlay};
  padding-bottom: 100px;
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  z-index: 10;
  width: ${({ width }) => width}px;
  height: ${({ height }) => height}px;
`;

const Lottie = styled(LottieView)`
  width: 350px;
  height: 350px;
`;

export const Loader = ({ style }) => {
  const { width, height } = useWindowDimensions();
  return (
    <Wrapper style={style} width={width} height={height}>
      <Lottie
        source={ANIMATIONS.loader}
        autoPlay
        loop={true}
        resizeMode="cover"
      />
    </Wrapper>
  );
};
