// Core Packages
import React from 'react';

// Styles
import { TabList, Tab, Label, Icon } from './styles';

export const ButtonTabs = ({ currentTab, setCurrentTab, tabs, style }) => {
  const updateTab = index => () => setCurrentTab(index);
  return (
    <TabList style={style}>
      {tabs.map(({ label, icon = null, disabled = false }, index) => (
        <Tab
          key={index}
          onPress={updateTab(index)}
          disabled={disabled}
          activeOpacity={0.5}
          active={currentTab !== index}
        >
          {icon ? <Icon>{icon()}</Icon> : null}
          <Label active={currentTab !== index}>{label || ' '}</Label>
        </Tab>
      ))}
    </TabList>
  );
};
