// Core Packages
import styled from 'styled-components/native';

// Utils
import { COLORS, SPACING, FONT_FAMILY } from '@utils/theme';

export const TabList = styled.View`
  flex-direction: row;
  flex: 1;
  border-radius: ${SPACING.small}px;
  background: ${COLORS.lightgrey};
`;
export const Tab = styled.TouchableOpacity`
  width: 100%;
  display: flex;
  flex: 1;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  opacity: ${({ disabled }) => (disabled ? 0.5 : 1)};
  border-radius: ${SPACING.small}px;
  padding: ${SPACING.default}px;
  background: ${({ active }) =>
    active ? COLORS.lightgrey : COLORS.alerzoBlue};
`;
export const Label = styled.Text`
  font-size: 16px;
  font-family: ${FONT_FAMILY.Gilmer500};
  width: 100%;
  color: ${({ active }) => (active ? COLORS.greyBlue : COLORS.white)};
  text-align: center;
`;
export const Icon = styled.View`
  margin-right: ${SPACING.small};
`;
