import styled from 'styled-components/native';
import { COLORS } from '@utils/theme';

const SelectInputWrapper = styled.TouchableOpacity`
  display: flex;
  width: 100%;
`;
const SelectInputLabel = styled.Text`
  font-family: GilmerBold;
  font-size: 14px;
  line-height: 17px;
  color: ${COLORS.fadeGrey};
  margin-bottom: 8px;
`;
const SelectPlaceHolder = styled.Text`
  font-family: 'GilmerRegular';
  font-size: 12px;
  line-height: 14px;
  color: ${COLORS.greyTwo};
`;
const SelectWrap = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  border: 0.8px solid ${COLORS.lightgrey};
  border-radius: 10px;
  height: 50px;
  margin-bottom: 16px;
  padding-right: 8px;
  padding-left: 8px;
`;
const CaretIcone = styled.Image`
  width: 24px;
  height: 24px;
`;
const ImageLabelWrap = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
`;

export {
  SelectInputWrapper,
  SelectInputLabel,
  SelectPlaceHolder,
  SelectWrap,
  ImageLabelWrap,
  CaretIcone,
};
