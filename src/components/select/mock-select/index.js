import { IMAGES } from '@utils/images';
import React, { useState } from 'react';
import { Image } from 'react-native';

// Utils
import {
  SelectInputWrapper,
  SelectInputLabel,
  ImageLabelWrap,
  SelectPlaceHolder,
  CaretIcone,
  SelectWrap,
} from './styles';

export const MockSelect = props => {
  const [isFocused, setIsFocused] = useState(false);
  // console.log(props.ImageSource, "ImageSource");
  return (
    <SelectInputWrapper isFocused={isFocused} onPress={props.onPress}>
      <SelectInputLabel>{props.selectLabel}</SelectInputLabel>
      <SelectWrap>
        <ImageLabelWrap>
          {props.ImageSource && <Image source={props.ImageSource} />}
          <SelectPlaceHolder>{props.selectPlaceholder}</SelectPlaceHolder>
        </ImageLabelWrap>
        <CaretIcone source={IMAGES.AngleDown} />
      </SelectWrap>
    </SelectInputWrapper>
  );
};
