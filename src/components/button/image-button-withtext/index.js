import React from 'react';
import styled from 'styled-components/native';
import { COLORS } from '@utils/theme';

const Wrapper = styled.TouchableOpacity`
background: #FFFFFF;
box-shadow: 0px 4px 30px rgba(0, 0, 0, 0.03);
border-radius: 10px;
height: 55px;
align-items: center;
background: ${COLORS.white};
flex-direction: row;
margin-bottom: 15px;
padding-right: 10px
padding-left: 10px
`;

const Img = styled.Image`
  height: 40px;
  width: 40px;
  margin-right: 13px;
`;
const ButtonText = styled.Text`
  font-family: 'GilmerRegular';
  font-style: normal;
  font-weight: 600;
  font-size: 16px;
  line-height: 17px;
  color: ${COLORS.fadeGrey};
`;

export const ButtonImageWithText = props => (
  <Wrapper onPress={props.onPress}>
    <Img source={props.source} />
    <ButtonText>{props.btnText}</ButtonText>
  </Wrapper>
);
