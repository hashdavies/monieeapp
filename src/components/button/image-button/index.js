import React from 'react';
import styled from 'styled-components/native';

const Wrapper = styled.TouchableOpacity``;

const Img = styled.Image``;

export const ImageButton = props => (
  <Wrapper onPress={props.onPress}>
    <Img source={props.source} />
  </Wrapper>
);
