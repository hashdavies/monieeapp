// Core Components
import React from 'react';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { COLORS, FONT_FAMILY, SPACING } from '@utils/theme';

// Utils
export { ImageButton } from './image-button';

export default function StyledButton({
  label,
  icon = null,
  onPress,
  variant = 'default', // outline, white, dark, light, text
  style = {},
  textStyle = {},
  disabled = false,
}) {
  return (
    <TouchableOpacity
      onPress={onPress}
      disabled={disabled}
      activeOpacity={0.5}
      style={[
        styles.button,
        disabled ? styles[`${variant}Disabled`] : styles[`${variant}Enabled`],
        style,
      ]}
    >
      {icon ? <View style={styles.iconWrap}>{icon()}</View> : null}
      <Text style={[styles.label, styles[`${variant}Label`], textStyle]}>
        {label || ' '}
      </Text>
    </TouchableOpacity>
  );
}

const styles = StyleSheet.create({
  button: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 16,
    padding: 16,
  },
  iconWrap: {
    marginRight: SPACING.small,
  },
  label: {
    fontSize: 14,
    lineHeight: 18,
    width: '100%',
    textAlign: 'center',
  },
  defaultLabel: {
    color: COLORS.white,
  },
  outlineLabel: {
    color: COLORS.white,
  },
  whiteLabel: {
    color: COLORS.alerzoBlue,
  },
  textBoldLabel: {
    color: COLORS.alerzoBlue,
  },
  textLabel: {
    color: COLORS.alerzoBlue,
    fontFamily: FONT_FAMILY.Gilmer400,
  },
  dangerLabel: {
    color: COLORS.alerzoRed,
  },
  lightLabel: {
    color: COLORS.alerzoBlue,
  },
  darkLabel: {
    color: COLORS.white,
  },
  primaryLabel: {
    fontSize: 16,
    fontFamily: FONT_FAMILY.Gilmer700,
    lineHeight: 19,
    color: COLORS.alerzoBlue,
    width: '100%',
    textAlign: 'center',
  },
  defaultEnabled: {
    backgroundColor: COLORS.btnDark,
  },
  defaultDisabled: {
    backgroundColor: COLORS.lightgrey,
  },
  outlineEnabled: {
    borderColor: COLORS.white,
    borderWidth: 1,
  },
  outlineDisabled: {
    borderColor: COLORS.white,
    borderWidth: 1,
  },
  whiteEnabled: {
    backgroundColor: COLORS.white,
  },
  whiteDisabled: {
    backgroundColor: COLORS.white,
  },
  textEnabled: {
    backgroundColor: COLORS.transparent,
  },
  textDisabled: {
    backgroundColor: COLORS.transparent,
  },
  dangerEnabled: {
    backgroundColor: COLORS.alerzoLightRed,
  },
  dangerDisabled: {
    backgroundColor: COLORS.alerzoLightRed,
  },
  darkEnabled: {
    backgroundColor: COLORS.greyThree,
  },
  darkDisabled: {
    backgroundColor: COLORS.greyFive,
  },
  lightEnabled: {
    backgroundColor: COLORS.alerzoLightBlue,
  },
  lightDisabled: {
    backgroundColor: COLORS.alerzoLightBlue,
  },
});
