// Core Packages
import styled from 'styled-components/native';
import Modal from 'react-native-modal';

// Utils
import { COLORS } from '@utils/theme';

export const StyledModal = styled(Modal)`
  margin: 0;
`;
export const ModalPane = styled.View`
  max-height: 95%;
  width: ${({ alignment }) => (alignment === 'center' ? '90%' : '100%')};
  margin: ${({ alignment }) =>
    alignment === 'center' ? 'auto' : 'auto 0 0 0'};
  position: relative;
  background-color: ${COLORS.white};
  border-radius: 10px;
  align-items: center;
  justify-content: center;
  overflow: hidden;
`;
export const ModalNudge = styled.View`
  position: absolute;
  top: 0;
  margin-horizontal: auto;
  padding-horizontal: 72px;
  padding-vertical: 12px;
  z-index: 100;
`;
export const CloseButton = styled.TouchableOpacity`
  position: absolute;
  top: 5px;
  right: 0;
  z-index: 200;
`;
