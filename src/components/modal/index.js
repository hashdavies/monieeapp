// Core Components
import React from 'react';
import { TouchableOpacity, ScrollView, View, Image } from 'react-native';

// Utils
import { IMAGES } from '@utils/images';

// Styles
import { StyledModal, ModalPane, ModalNudge, CloseButton } from './styles';

export const Modal = ({
  isVisible,
  children,
  style,
  alignment = 'bottom', // "center"
  onClose = () => {},
  dismissible = true,
  swipeEnabled = true,
  closeOnBackdropPress = true,
}) => {
  const closeModal = dismissible ? onClose : () => {};
  const backdropPress = closeOnBackdropPress ? closeModal : () => {};
  const onSwipe = swipeEnabled ? closeModal : () => {};

  return (
    <StyledModal
      alignment={alignment}
      swipeDirection={swipeEnabled ? 'down' : null}
      onSwipeComplete={onSwipe}
      isVisible={isVisible}
      onBackdropPress={backdropPress}
      onBackButtonPress={closeModal}
      scrollHorizontal
      propagateSwipe={true}
      avoidKeyboard
    >
      {children ? (
        <ModalPane alignment={alignment}>
          {swipeEnabled ? (
            <ModalNudge>
              <Image source={IMAGES.MODAL_NUDGE} />
            </ModalNudge>
          ) : null}
          {dismissible ? (
            <CloseButton onPress={onClose}>
              <Image source={IMAGES.Close} />
            </CloseButton>
          ) : null}
          <ScrollView>
            <TouchableOpacity activeOpacity={1}>
              <View style={style}>{children}</View>
            </TouchableOpacity>
          </ScrollView>
        </ModalPane>
      ) : null}
    </StyledModal>
  );
};
