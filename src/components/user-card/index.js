import React from 'react';
import styled from 'styled-components/native';
import { COLORS } from '@utils/theme';

const Wrapper = styled.TouchableOpacity`
box-shadow: 0px 4px 30px rgba(0, 0, 0, 0.03);
border-radius: 10px;
align-items: center;
justify-content: center;
background: ${COLORS.white};
padding: 10px
margin-left: 10px;
width: 83px;
`;

const Img = styled.Image`
  height: 40px;
  width: 40px;
`;
const UserNameText = styled.Text`
  font-family: 'GilmerRegular';
  font-size: 10px;
  line-height: 12px;
  color: ${COLORS.greyThree};
  margin-top: 6px;
  margin-bottom: 5px;
  width: 100%;
`;
const UserNumber = styled.Text`
  font-family: 'GilmerRegular';
  font-size: 10px;
  line-height: 12px;
  width: 100%;
  color: ${COLORS.greyThree};
`;

export const UserCard = ({ userName, userNumber, source, onPress, style }) => (
  <Wrapper onPress={onPress} style={style}>
    <Img source={source} />
    <UserNameText numberOfLines={1}>{userName}</UserNameText>
    <UserNumber numberOfLines={1}>{userNumber}</UserNumber>
  </Wrapper>
);
