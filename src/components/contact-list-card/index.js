import { IMAGES } from '@utils/images';
import React, { useState } from 'react';

// Utils
import {
  InputWrapper,
  StyledInput,
  ImageWrapper,
  AbbreviatedText,
  NameWrapper,
  ContactName,
  ContactMiniDetails,
  Image,
  CheckButton,
  View,
} from './styles';

export const ContactList = ({
  abbreviatedText,
  ImageSource,
  contactName,
  miniDetail,
  activate,
}) => {
  const [isFocused, setIsFocused] = useState(false);
  return (
    <InputWrapper isFocused={isFocused}>
      <View>
        {ImageSource !== '' ? (
          <Image source={ImageSource} />
        ) : (
          <ImageWrapper>
            <AbbreviatedText>{abbreviatedText} </AbbreviatedText>
          </ImageWrapper>
        )}
        <NameWrapper>
          <ContactName>{contactName}</ContactName>
          <ContactMiniDetails>{miniDetail}</ContactMiniDetails>
        </NameWrapper>
      </View>
      {activate === true ? (
        <CheckButton source={IMAGES.tickCircle} />
      ) : (
        <CheckButton source={IMAGES.UnTickCircle} />
      )}
    </InputWrapper>
  );
};
