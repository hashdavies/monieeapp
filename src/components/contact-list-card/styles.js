import styled from 'styled-components/native';
import { COLORS } from '@utils/theme';

const InputWrapper = styled.TouchableOpacity`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  margin-bottom: 20px;
`;

const StyledInput = styled.TextInput`
  flex: 0.9;
  font-style: normal;
  font-weight: 600;
  font-size: 12px;
  line-height: 14px;
`;

const ImageWrapper = styled.View`
  background: ${COLORS.alerzoBlueTwo}
  width: 40px;
  height: 40px;
  border-radius:20px;
  align-items: center;
  justify-content: center;
`;
const Image = styled.Image`
  width: 40px;
  height: 40px;
  border-radius: 20px;
  align-items: center;
  justify-content: center;
`;
const CheckButton = styled.Image`
  width: 24px;
  height: 24px;
`;
const NameWrapper = styled.View`
  display: flex;
  margin-left: 10px;
`;
const View = styled.View`
  display: flex;
  flex-direction: row;
  align-items: center;
`;
export const AbbreviatedText = styled.Text`
  font-family: 'GilmerRegular';
  font-style: normal;
  font-weight: 700;
  font-size: 14px;
  line-height: 17px;
  color: ${COLORS.backgroundTwo};
`;
const ContactName = styled.Text`
  font-family: 'GilmerRegular';
  font-style: normal;
  font-weight: 600;
  font-size: 14px;
  line-height: 17px;
  color: ${COLORS.greyThree};
`;
const ContactMiniDetails = styled.Text`
  font-family: 'GilmerRegular';
  font-style: normal;
  font-weight: 400;
  font-size: 12px;
  line-height: 14px;
  color: ${COLORS.accountNumber};
`;

export {
  InputWrapper,
  StyledInput,
  ImageWrapper,
  // AbbreviatedText as AbbrivatedText,
  ContactName,
  ContactMiniDetails,
  NameWrapper,
  Image,
  CheckButton,
  View,
};
