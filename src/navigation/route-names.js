export const ROUTE_NAMES = {
  ON_BOARDING: 'Onboarding',
  VERIFY: 'verify',
  BVN: 'bvn',
  SETPIN: 'SetPin',
  CONFIRM_PIN: 'ConfirmPin',
  DASHBOARD: 'Dashboard',
  REQUEST: 'Request',
  SEND: 'Send',
  REQUEST_SEND: 'RequestSent',
  FUND_SENT: 'FundSent',
  SIGN_IN: 'SignIn',
  ENTER_PIN: 'EnterPin',
};
