// Core Packages
import React from 'react';
import { createNativeStackNavigator } from '@react-navigation/native-stack';

// Screens
import { OnBoarding } from '@screens/onboarding';
import { SignIn } from '@screens/signIn';
import { Dashboard } from '@screens/dashboard/index';
import { Request } from '@screens/request';
import { Send } from '@screens/send';
import { RequestSent } from '@screens/requestSent';
import { FundSent } from '@screens/fundSent';
import { EnterPin } from '@screens/signIn/enterPin';
import { Verify } from '@screens/onboarding/verify';
import { Bvn } from '@screens/onboarding/bvn';
import { SetPin } from '@screens/onboarding/setpin';
import { ConfirmPin } from '@screens/onboarding/confirmPin';

// Custom Components
import { ROUTE_NAMES } from './route-names';

// Utils
import { COLORS, FONT_FAMILY } from '@utils/theme';

const Stack = createNativeStackNavigator();

export const MainStack = () => (
  <Stack.Navigator
    screenOptions={{
      gestureEnabled: false,
      headerTintColor: COLORS.greyBlue,
      headerStyle: {
        backgroundColor: COLORS.lightBlueFive,
      },
      headerTitleStyle: {
        fontFamily: FONT_FAMILY.Gilmer600,
        fontSize: 16,
      },
    }}
    initialRouteName={ROUTE_NAMES.ON_BOARDING}>
    <Stack.Screen
      name={ROUTE_NAMES.ON_BOARDING}
      component={OnBoarding}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name={ROUTE_NAMES.VERIFY}
      component={Verify}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name={ROUTE_NAMES.BVN}
      component={Bvn}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name={ROUTE_NAMES.SETPIN}
      component={SetPin}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name={ROUTE_NAMES.CONFIRM_PIN}
      component={ConfirmPin}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name={ROUTE_NAMES.SIGN_IN}
      component={SignIn}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name={ROUTE_NAMES.ENTER_PIN}
      component={EnterPin}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name={ROUTE_NAMES.DASHBOARD}
      component={Dashboard}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name={ROUTE_NAMES.REQUEST}
      component={Request}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name={ROUTE_NAMES.SEND}
      component={Send}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name={ROUTE_NAMES.REQUEST_SEND}
      component={RequestSent}
      options={{ headerShown: false }}
    />
    <Stack.Screen
      name={ROUTE_NAMES.FUND_SENT}
      component={FundSent}
      options={{ headerShown: false }}
    />
  </Stack.Navigator>
);
