// Core Packages
import React, { useContext, useCallback, useEffect } from 'react';

// Custom Component
import Button from '@components/button';
import { OnBoardHead } from '@components/onboardHead';
import Input from '@components/input';

// Utils
import { ROUTE_NAMES } from '@navigation/route-names';
import { Wrapper, GroupView } from './styles';
import { COLORS } from '@utils/theme';

export const SignIn = ({ navigation }) => {
  useEffect(() => {
    // ;
  }, []);

  return (
    <Wrapper>
      <GroupView>
        <OnBoardHead
          title={'Let’s begin'}
          subTitle={'Enter your phone number'}
        />
        <Input placeholder={'Phone Number'} keyboardType="phone" />
      </GroupView>
      <Button
        label={'Continue'}
        onPress={() => navigation.navigate(ROUTE_NAMES.ENTER_PIN)}
      />
    </Wrapper>
  );
};
