// Core Packages
import React, { useContext, useCallback, useEffect } from 'react';

// Custom Component
import Slider from './slider';
import SliderItem from './slider/slider-item';
import Button from '@components/button';
import { OnBoardHead } from '@components/onboardHead';
import Input from '@components/input';
import { Loader } from '@components/loader';

// Utils
import { ANIMATIONS } from '@assets/animations';
import { AuthContext } from '@contexts/auth';
import { ROUTE_NAMES } from '@navigation/route-names';
import { ProgressBar } from 'react-native-stories-view';
import { StatusBar } from 'react-native';
import { useIsFocused } from '@react-navigation/native';
import { Wrapper, GroupView } from './styles';
import { COLORS } from '@utils/theme';

export const OnBoarding = ({ navigation }) => {
  useEffect(() => {
    // ;
  }, []);

  return (
    <Wrapper>
      <GroupView>
        <OnBoardHead
          title={'Let’s begin'}
          subTitle={'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'}
        />
        <Input placeholder={'Phone Number'} keyboardType="phone" />
      </GroupView>
      <Button
        label={'Get Started'}
        onPress={() => navigation.navigate(ROUTE_NAMES.VERIFY)}
      />
    </Wrapper>
  );
};
