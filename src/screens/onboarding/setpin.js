// Core Packages

import React, { useEffect } from 'react';

import { Otp } from '@components/otp';

// Utils

import { ROUTE_NAMES } from '@navigation/route-names';

export const SetPin = ({ navigation }) => {
  useEffect(() => {
    // ;
  }, []);

  return (
    <Otp
      title={'Set PIN'}
      routeTo={ROUTE_NAMES.CONFIRM_PIN}
      navigation={navigation}
      subTitle={'Facilisis mauris, potenti vitae cras risus.'}
    />
  );
};
