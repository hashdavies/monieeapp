// Core Packages
import React, { useContext, useCallback, useEffect } from 'react';

// Custom Component
import Slider from './slider';
import SliderItem from './slider/slider-item';
import Button from '@components/button';
import { OnBoardHead } from '@components/onboardHead';
import Input from '@components/input';
import { Loader } from '@components/loader';
import { IMAGES } from '@utils/images';

// Utils
import { ANIMATIONS } from '@assets/animations';
import { AuthContext } from '@contexts/auth';
import { ROUTE_NAMES } from '@navigation/route-names';
import { ProgressBar } from 'react-native-stories-view';
import { StatusBar } from 'react-native';
import { useIsFocused } from '@react-navigation/native';
import { Wrapper, GroupView } from './styles';
import { COLORS } from '@utils/theme';

export const Bvn = ({ navigation }) => {
  useEffect(() => {
    // ;
  }, []);

  return (
    <Wrapper>
      <GroupView>
        <OnBoardHead
          imageLink={IMAGES.ARROW_LEFT}
          title={'Add your bank details'}
          subTitle={'Kindly ensure the details you enter belong to you.'}
        />
        <Input placeholder={'Bank'} keyboardType="phone" />
        <Input placeholder={'Account Number'} keyboardType="phone" />
        <Input placeholder={'BVN '} keyboardType="phone" />
      </GroupView>
      <Button
        label={'Save and Continue'}
        onPress={() => navigation.navigate(ROUTE_NAMES.SETPIN)}
      />
    </Wrapper>
  );
};
