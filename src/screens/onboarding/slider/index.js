import React from 'react';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { ROUTE_NAMES } from '@navigation/route-names';
import SliderItem from './slider-item';
import { ANIMATIONS } from '@assets/animations';
import styled from 'styled-components/native';

const Tab = createMaterialTopTabNavigator();

const Container = styled.View`
  flex: 1;
  width: 100%;
`;

const Slider = () => (
  <Container>
    <Tab.Navigator
      screenOptions={{
        tabBarStyle: { height: 0 },
        headerShown: false,
      }}
    >
      <Tab.Screen
        name={ROUTE_NAMES.ON_BOARDING_SLIDER_1}
        children={() => (
          <SliderItem
            source={ANIMATIONS.onBoardingScreen1}
            subTitle="Money should never stop your shine"
          />
        )}
      />
      <Tab.Screen
        name={ROUTE_NAMES.ON_BOARDING_SLIDER_2}
        children={() => (
          <SliderItem
            source={ANIMATIONS.onBoardingScreen2}
            subTitle="Budget your spend with ease 🤑"
          />
        )}
      />
      <Tab.Screen
        name={ROUTE_NAMES.ON_BOARDING_SLIDER_3}
        children={() => (
          <SliderItem
            source={ANIMATIONS.onBoardingScreen3}
            subTitle="Scan your expenses at a glance ✌🏾"
          />
        )}
      />
      <Tab.Screen
        name={ROUTE_NAMES.ON_BOARDING_SLIDER_4}
        children={() => (
          <SliderItem
            source={ANIMATIONS.onBoardingScreen4}
            subTitle="Shine 🌟 on with smart banking"
          />
        )}
      />
    </Tab.Navigator>
  </Container>
);

export default Slider;
