// Core
import React, { useEffect, useRef } from 'react';
import { ImageBackground, useWindowDimensions } from 'react-native';

// Utils
import {
  Wrapper,
  AnimatedLottie,
  SubTitleWrapper,
  SubTitle,
  backgroundImageStyle,
} from './styles';

// const backgroundImageStyle = {
//   flex: 1,
//   width: '100%',
// };

const SliderItem = ({ source, subTitle }) => {
  const { width } = useWindowDimensions();
  const ref = useRef();
  useEffect(() => {
    ref.current.play();
  }, []);

  return (
    <ImageBackground
      source={require('../../../../../assets/images/slider-bg.png')}
      style={backgroundImageStyle}
      resizeMode={'cover'}>
      <Wrapper>
        <AnimatedLottie ref={ref} width={width} autoplay source={source} loop />
        <SubTitleWrapper>
          <SubTitle>{subTitle}</SubTitle>
        </SubTitleWrapper>
      </Wrapper>
    </ImageBackground>
  );
};

export default SliderItem;
