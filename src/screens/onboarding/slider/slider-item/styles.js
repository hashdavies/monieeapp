import LottieView from 'lottie-react-native';
import styled from 'styled-components/native';
import { H4 } from '@components/text';

const Wrapper = styled.View`
  flex: 1;
  justify-content: space-between;
  background-color: transparent;
  align-items: center;
`;

const AnimatedLottie = styled(LottieView)`
  width: ${({ width }) => width}px;
  background-color: transparent;
`;

const SubTitleWrapper = styled.View`
  align-items: baseline;
  justify-content: center;
`;

const SubTitle = styled(H4)`
  padding-horizontal: 24px;
  text-align: center;
  color: white;

  font-family: 'GilmerBold';
  font-size: 32px;
  line-height: 38px;
`;

const backgroundImageStyle = {
  flex: 1,
  width: '100%',
};

export {
  Wrapper,
  AnimatedLottie,
  SubTitleWrapper,
  SubTitle,
  backgroundImageStyle,
};
