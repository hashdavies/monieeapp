// Core Packages
import React, { useContext, useCallback, useEffect, useState } from 'react';

// Custom Component
import Slider from './slider';
import SliderItem from './slider/slider-item';
import Button from '@components/button';
import { OnBoardHead } from '@components/onboardHead';
import Input from '@components/input';
import { PinInput } from '@components/pin-input';
import { Loader } from '@components/loader';

// Utils
import { ANIMATIONS } from '@assets/animations';
import { AuthContext } from '@contexts/auth';
import { ROUTE_NAMES } from '@navigation/route-names';
import { ProgressBar } from 'react-native-stories-view';
import { StatusBar, View } from 'react-native';
import { useIsFocused } from '@react-navigation/native';
import {
  Wrapper,
  GroupView,
  VerifyText,
  CounterText,
  GroupViewer,
  InputConfirmWrapper,
} from './styles';
import { COLORS } from '@utils/theme';
import { IMAGES } from '@utils/images';
import CodeInput from 'react-native-code-input';
// import { View } from '@components/action-tile/styles';
const CELL_COUNT = 4;
export const Verify = ({ navigation }) => {
  const [value, setValue] = useState();
  useEffect(() => {
    // ;
  }, []);
  const _onFulfill = () => {
    navigation.navigate(ROUTE_NAMES.BVN);
  };
  return (
    <Wrapper>
      <GroupView>
        <OnBoardHead
          imageLink={IMAGES.ARROW_LEFT}
          title={'Verification'}
          subTitle={'Lorem ipsum dolor sit amet, consectetur adipiscing elit.'}
        />

        <View style={{ height: 100 }}>
          <CodeInput
            space={5}
            size={60}
            inputPosition="center"
            codeLength={4}
            onFulfill={code => _onFulfill(code)}
            activeColor="#000"
            inactiveColor="#FAFAFA"
            borderType="square"
            keyboardType="numeric"
            codeInputStyle={{
              borderWidth: 1.5,
              backgroundColor: 'FAFAFA',
            }}
          />
        </View>

        <VerifyText>Verification code will be resent in</VerifyText>
        <CounterText>0:54</CounterText>
      </GroupView>

      <Button
        label={'Submit'}
        onPress={() => navigation.navigate(ROUTE_NAMES.BVN)}
      />
    </Wrapper>
  );
};
