// Core Packages
import styled from 'styled-components/native';
import { COLORS } from '@utils/theme';

export const Wrapper = styled.View`
  flex: 1;
  justify-content: space-between;
  align-items: center;
  background: ${COLORS.white};
  width: 100%;
  padding-top: 60px
  padding-bottom: 41px
  padding-left: 24px
  padding-right: 24px
`;
export const GroupView = styled.View`
  // display: flex;
  // width: 100%;
`;
export const GroupViewer = styled.View`
  // background-color: red;
  // margin-top: -500px;
`;
export const VerifyText = styled.Text`
  font-family: 'ABeeZee';
  font-style: normal;
  font-weight: 400;
  font-size: 12px;
  line-height: 14px;
  text-align: center;
  // margin-top: 20px;
  margin-bottom: 8px;

  color: ${COLORS.fadeDark2};
`;
export const CounterText = styled.Text`
  font-family: 'ABeeZee';
  font-style: italic;
  font-weight: 400;
  font-size: 14px;
  line-height: 16px;
  text-align: center;
  color: ${COLORS.btnDark};
`;
export const InputConfirmWrapper = styled.View`
  // flex: 1;
  justify-content: center;
  align-items: center;
  // background-color: red;
`;
