// Core Packages
import styled from 'styled-components/native';
import { COLORS } from '@utils/theme';

export const Wrapper = styled.View`
  flex: 1;
  justify-content: space-between;
  align-items: center;
  background: ${COLORS.white};
  width: 100%;
  padding-top: 30px
  padding-bottom: 10px
  padding-left: 24px
  padding-right: 24px
`;
export const FLexTab = styled.View`
  flex-direction: row;
  // justify-content: space-between;
  align-items: center;
  background: ${COLORS.white};
  width: 100%;
  height: 34px;
`;
export const Flex = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
`;
export const FlexCenter = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
  width: 100%;
`;
export const FlexCenterAlignTo = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: flex-start;
  width: 100%;
`;
export const WalletBalWrap = styled.View`
  margin-top: 10px;
  margin-bottom: 30px;
  width: 178px;
  height: 62px;
  background: #fafafa;
  flex-direction: row;
  align-items: center;
  border: 1px solid #f2f2f2;
  border-radius: 12px;
  padding: 8px 40px 8px 16px;
`;
export const CircleShape = styled.View`
  width: 40px;
  height: 40px;
  border-radius: 20px;
  background: #e0e0e0;
`;

export const PageLabel = styled.Text`
  font-family: 'Nexa';
  font-style: normal;
  font-weight: 800;
  font-size: 17px;
  line-height: 16px;
  color: #0c0c26;
`;
export const WalletBalLabel = styled.Text`
  font-family: 'ABeeZee';
  font-style: normal;
  font-weight: 400;
  font-size: 12px;
  line-height: 15px;
  text-align: center;
  color: #000000;
`;
export const WalletBalValue = styled.Text`
  font-family: 'Nexa';
  font-style: normal;
  font-weight: 800;
  font-size: 17px;
  line-height: 17px;
  text-align: center;
  color: #0c0c26;
`;
export const AmtSymbolWrap = styled.Text`
  font-family: 'Abhaya Libre Medium';
  font-style: normal;
  font-weight: 500;
  font-size: 36px;
  line-height: 42px;
  text-align: center;
  color: #0c0c26;
`;
export const ValueEntered = styled.Text`
  font-family: 'Abhaya Libre Medium';
  font-style: normal;
  font-weight: 500;
  font-size: 64px;
  line-height: 68px;
  margin-left: 8px;
  text-align: center;
  color: #0c0c26;
`;
export const ImageHolder = styled.View`
width: 58px
height: 58px;
flex-direction: row;
justify-content: center;
align-items: center;
position: relative;
`;
export const ContactImage = styled.Image`
width: 48px
height: 48px;
border-radius: 24px
`;

export const FLexTabImage = styled.Image`
  width: 24px
  height: 24px;
`;
export const FLexTabTopImage = styled.Image`
  width: 32px
  height: 32px;
`;
export const ContactCloseImage = styled.Image`
  width: 20px
  height: 20px;
  position: relative
  margin-left : 5px
`;
export const ContactCloseImage2 = styled.Image`
  width: 20px
  height: 20px;
  position: absolute;
  right: -1px
  top:0px;

`;
export const ActionBtn = styled.TouchableOpacity`
  width: 123px;
  height: 43px;
  justify-content: center;
  align-items: center;
  background: #f2f2f2;

  border-radius: 12px;
`;
export const ActionBtnText = styled.Text`
  // font-family: "Nexa";
  font-style: normal;
  font-weight: 800;
  font-size: 14px;
  line-height: 17px;
  text-align: center;
  color: #828282;
`;
export const GroupView = styled.View`
  display: flex;
  width: 100%;
  justify-content: center;
  align-items: center;
`;
export const GroupViewer = styled.View`
  display: flex;
  width: 100%;
`;
export const VerifyText = styled.Text`
  font-family: 'ABeeZee';
  font-style: normal;
  font-weight: 400;
  font-size: 12px;
  line-height: 14px;
  text-align: center;
  margin-top: 20px;
  margin-bottom: 8px;

  color: ${COLORS.fadeDark2};
`;
export const CounterText = styled.Text`
  font-family: 'ABeeZee';
  font-style: italic;
  font-weight: 400;
  font-size: 14px;
  line-height: 16px;
  text-align: center;
  color: ${COLORS.btnDark};
`;
export const StyledPinInput = styled.View`
  margin-top: 40px;
  margin-bottom: 40px;
`;
export const SelectContactHolder = styled.View`
  width: 100%;
  background: #fafafa;
  border-radius: 8px;
  height: 48px;
  flex-direction: row;
  align-items: center;
  padding-left: 10px;
  padding-right: 10px;
`;
export const SelectContactHolder2 = styled.View`
  flex-direction: row;
  width: 100%;
  align-items: center;
  margin-top: 20px;
  margin-bottom: 20px;
  padding-left: 10px;
  padding-right: 10px;
`;
export const NameHolder = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
`;
export const NameText = styled.Text`
  font-family: 'ABeeZee';
  font-style: normal;
  font-weight: 400;
  font-size: 12px;
  line-height: 15px;
  color: #4f4f4f;
`;
export const Hr = styled.View`
  margin-top: 20px;
  margin-bottom: 40px;
  border: 0.2px #ccc;
`;
