// Core Packages
import styled from 'styled-components/native';
import { COLORS } from '@utils/theme';

export const Wrapper = styled.View`
  flex: 1;
  justify-content: space-between;
  align-items: center;
  background: ${COLORS.white};
  width: 100%;
  padding-top: 30px
  padding-bottom: 10px
  padding-left: 24px
  padding-right: 24px
`;
export const FLexTab = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  background: ${COLORS.white};
  width: 100%;
  height: 34px;
`;
export const Flex = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  width: 100%;
`;
export const FlexCenter = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: center;
  width: 100%;
`;
export const FlexCenterAlignTo = styled.View`
  flex-direction: row;
  justify-content: center;
  align-items: flex-start;
  width: 100%;
`;
export const WalletBalWrap = styled.View`
  margin-top: 10px;
  width: 119px;
  height: 62px;
  background: #fafafa;
  justify-content: center;
  align-items: center;
  border: 1px solid #f2f2f2;
  border-radius: 12px;
`;
export const WalletBalLabel = styled.Text`
  font-family: 'ABeeZee';
  font-style: normal;
  font-weight: 400;
  font-size: 12px;
  line-height: 15px;
  text-align: center;
  color: #828282;
`;
export const WalletBalValue = styled.Text`
  font-family: 'ABeeZee';
  font-style: italic;
  font-weight: 400;
  font-size: 17px;
  line-height: 20px;
  text-align: center;
  color: #0c0c26;
  margin-top: 4px;
`;
export const AmtSymbolWrap = styled.Text`
  font-family: 'Abhaya Libre Medium';
  font-style: normal;
  font-weight: 500;
  font-size: 36px;
  line-height: 42px;
  text-align: center;
  color: #0c0c26;
`;
export const ValueEntered = styled.Text`
  font-family: 'Abhaya Libre Medium';
  font-style: normal;
  font-weight: 500;
  font-size: 64px;
  line-height: 68px;
  margin-left: 8px;
  text-align: center;
  color: #0c0c26;
`;

export const FLexTabImage = styled.Image`
  width: 24px
  height: 24px;
`;
export const FLexTabTopImage = styled.Image`
  width: 32px
  height: 32px;
`;
export const ActionBtn = styled.TouchableOpacity`
  width: 123px;
  height: 43px;
  justify-content: center;
  align-items: center;
  background: #f2f2f2;

  border-radius: 12px;
`;
export const ActionBtnText = styled.Text`
  // font-family: "Nexa";
  font-style: normal;
  font-weight: 800;
  font-size: 14px;
  line-height: 17px;
  text-align: center;
  color: #828282;
`;
export const GroupView = styled.View`
  display: flex;
  width: 100%;
  justify-content: center;
  align-items: center;
`;
export const GroupViewer = styled.View`
  display: flex;
  width: 100%;
`;
export const VerifyText = styled.Text`
  font-family: 'ABeeZee';
  font-style: normal;
  font-weight: 400;
  font-size: 12px;
  line-height: 14px;
  text-align: center;
  margin-top: 20px;
  margin-bottom: 8px;

  color: ${COLORS.fadeDark2};
`;
export const CounterText = styled.Text`
  font-family: 'ABeeZee';
  font-style: italic;
  font-weight: 400;
  font-size: 14px;
  line-height: 16px;
  text-align: center;
  color: ${COLORS.btnDark};
`;
