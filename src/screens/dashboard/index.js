// Core Packages
import React, { useContext, useCallback, useEffect, useState } from 'react';

// Custom Component
import { DialPad } from '@components/dial-pad';
import { IMAGES } from '@utils/images';

// Utils
import { ROUTE_NAMES } from '@navigation/route-names';
import {
  Wrapper,
  FLexTab,
  FLexTabImage,
  FLexTabTopImage,
  ActionBtn,
  ActionBtnText,
  Flex,
  FlexCenter,
  FlexCenterAlignTo,
  WalletBalWrap,
  WalletBalLabel,
  WalletBalValue,
  AmtSymbolWrap,
  ValueEntered,
} from './styles';
import { COLORS } from '@utils/theme';
import { TouchableOpacity, View } from 'react-native';

export const Dashboard = ({ navigation }) => {
  const [pin, setPin] = useState('');

  const handleInput = digit => {
    setPin(pin + digit);
  };

  return (
    <Wrapper>
      <FLexTab>
        <TouchableOpacity>
          <FLexTabTopImage source={IMAGES.ScanBarcode} />
        </TouchableOpacity>
        <TouchableOpacity>
          <FLexTabTopImage source={IMAGES.Clock} />
        </TouchableOpacity>
      </FLexTab>
      <View style={{ width: '100%' }}>
        <FlexCenterAlignTo>
          <AmtSymbolWrap>₦</AmtSymbolWrap>
          <ValueEntered>0</ValueEntered>
        </FlexCenterAlignTo>
        <FlexCenter>
          <WalletBalWrap>
            <WalletBalLabel>Wallet Balance</WalletBalLabel>
            <WalletBalValue>5,200</WalletBalValue>
          </WalletBalWrap>
        </FlexCenter>
        <DialPad
          onPress={digit => handleInput(digit)}
          useDot={true}
          showBiometric={false}
        />

        <Flex>
          <ActionBtn onPress={() => navigation.navigate(ROUTE_NAMES.REQUEST)}>
            <ActionBtnText>Request</ActionBtnText>
          </ActionBtn>
          <ActionBtn onPress={() => navigation.navigate(ROUTE_NAMES.SEND)}>
            <ActionBtnText>Send</ActionBtnText>
          </ActionBtn>
        </Flex>
      </View>
      <FLexTab>
        <TouchableOpacity>
          <FLexTabImage source={IMAGES.Home2} />
        </TouchableOpacity>
        <TouchableOpacity>
          <FLexTabImage source={IMAGES.Category} />
        </TouchableOpacity>
        <TouchableOpacity>
          <FLexTabImage source={IMAGES.Profile} />
        </TouchableOpacity>
      </FLexTab>
    </Wrapper>
  );
};
