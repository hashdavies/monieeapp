// Core Packages
import React from 'react';
import { Card } from '@components/card';

// Utils
import { ROUTE_NAMES } from '@navigation/route-names';
import { IMAGES } from '@utils/images';

export const FundSent = ({ navigation }) => {
  return (
    <Card
      source={IMAGES.Vector}
      title={'Money sent'}
      subTitle={'Your request for ₦2,000 to Terry'}
      subTitle2={'and 5 others has been sent'}
      btnLabel={'Kari me go house'}
      onPress={() => navigation.navigate(ROUTE_NAMES.DASHBOARD)}
    />
  );
};
