// Core Packages
import React from 'react';
import { Card } from '@components/card';

// Utils
import { ROUTE_NAMES } from '@navigation/route-names';
import { IMAGES } from '@utils/images';

export const RequestSent = ({ navigation }) => {
  return (
    <Card
      source={IMAGES.Vector}
      title={'Request sent'}
      subTitle={'Your request for ₦2,000 to Terry'}
      subTitle2={'and 5 others has been sent'}
      btnLabel={'Go Home'}
      onPress={() => navigation.navigate(ROUTE_NAMES.DASHBOARD)}
    />
  );
};
