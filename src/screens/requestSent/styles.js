// Core Packages
import styled from 'styled-components/native';
import { COLORS } from '@utils/theme';

export const Wrapper = styled.View`
  flex: 1;
  justify-content: space-between;
  align-items: center;
  background: ${COLORS.white};
  width: 100%;
  padding-top: 30px
  padding-bottom: 10px
  padding-left: 24px
  padding-right: 24px
`;
export const WalletBalValue = styled.Text`
  font-family: 'Nexa';
  font-style: normal;
  font-weight: 800;
  font-size: 17px;
  line-height: 17px;
  text-align: center;
  color: #0c0c26;
  margin-top: 24px;
  margin-bottom: 12px;
`;
export const WalletBalLabel = styled.Text`
  font-family: 'ABeeZee';
  font-style: normal;
  font-weight: 400;
  font-size: 14px;
  line-height: 15px;
  text-align: center;
  color: #4f4f4f;
  margin-bottom: 5px;
`;
export const FLexTabTopImage = styled.Image`
  width: 189px;
  height: 160px;
`;
export const FlexCenter = styled.View`
  justify-content: center;
  align-items: center;
  width: 100%;
  flex: 1;
`;
