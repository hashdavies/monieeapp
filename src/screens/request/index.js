// Core Packages
import React, { useContext, useCallback, useEffect, useState } from 'react';

// Custom Component
import Button from '@components/button';
import { Card } from '@components/card';
import { DialPad } from '@components/dial-pad';
import { ContactList } from '@components/contact-list-card';
import { Modal } from '@components/modal';
import Input from '@components/input';
import { InputWithLabel } from '@components/input/input-with-label';
import { IMAGES } from '@utils/images';
// import { Otp } from "@components/otp";
import { PinInput } from '@components/pin-input/plain-pin-input';
import { Dimensions } from 'react-native';

// Utils
import { ROUTE_NAMES } from '@navigation/route-names';
import {
  Wrapper,
  FLexTab,
  FLexTabImage,
  FLexTabTopImage,
  ActionBtn,
  ActionBtnText,
  Flex,
  FlexCenter,
  FlexCenterAlignTo,
  WalletBalWrap,
  WalletBalLabel,
  WalletBalValue,
  AmtSymbolWrap,
  ValueEntered,
  PageLabel,
  CircleShape,
  StyledPinInput,
  SelectContactHolder2,
  SelectContactHolder,
  Hr,
  ContactCloseImage,
  ContactCloseImage2,
  ImageHolder,
  ContactImage,
  NameHolder,
  NameText,
} from './styles';
import { COLORS } from '@utils/theme';
import { TouchableOpacity, View } from 'react-native';
import { UserContactList } from '@constants/index';
const PIN_SIZE = 4;
const windowWidth = Dimensions.get('window').width;

export const Request = ({ navigation }) => {
  const [showModal, setShowModal] = useState(false);
  const [showContactModal, setShowContactModal] = useState(false);
  const [showAccessModal, setShowAccessModal] = useState(false);
  const [pin, setPin] = useState('');
  useEffect(() => {
    if (pin.length === PIN_SIZE) {
      navigation.navigate(ROUTE_NAMES.REQUEST_SEND);
    }
  }, [navigation, pin]);
  const processUserAcceptAccess = () => {
    setShowAccessModal(false);
    setShowContactModal(true);
  };
  const handleInput = digit => {
    setPin(pin + digit);
  };
  return (
    <Wrapper>
      <View style={{ width: '100%' }}>
        <FLexTab>
          <TouchableOpacity>
            <FLexTabTopImage source={IMAGES.ARROW_LEFT} />
          </TouchableOpacity>
          <PageLabel>Request Money</PageLabel>
        </FLexTab>
        <FlexCenter>
          <WalletBalWrap>
            <CircleShape />
            <View style={{ width: '70%' }}>
              <WalletBalLabel>Request</WalletBalLabel>
              <FlexCenterAlignTo>
                <WalletBalValue>₦</WalletBalValue>
                <WalletBalValue>15,000</WalletBalValue>
              </FlexCenterAlignTo>
            </View>
          </WalletBalWrap>
        </FlexCenter>
        <Input placeholder={'Purpose for sending'} keyboardType="phone" />
        <InputWithLabel
          placeholder={'Phone Number'}
          keyboardType="phone"
          source={IMAGES.Book}
          onPress={() => setShowAccessModal(!showAccessModal)}
          // rightAdornment={<FLexTabTopImage source={IMAGES.ARROW_LEFT} />}
        />
      </View>
      <Button
        label={'Request'}
        disable
        onPress={() => setShowModal(!showModal)}
      />
      <Modal isVisible={showModal} onClose={() => setShowModal(!showModal)}>
        <View
          style={{
            marginTop: 40,
            height: '100%',
            width: windowWidth - 40,
            flex: 1,
          }}>
          <WalletBalValue>Enter PIN</WalletBalValue>
          <StyledPinInput>
            <PinInput pin={pin} size={PIN_SIZE} />
          </StyledPinInput>
          <DialPad
            onPress={digit => handleInput(digit)}
            onDeletePress={() => pin && setPin(pin.slice(0, -1))}
            showBiometric={false}
            useDot={false}
          />
        </View>
      </Modal>
      <Modal
        isVisible={showAccessModal}
        onClose={() => setShowAccessModal(!showAccessModal)}>
        <View
          style={{
            marginTop: 40,
            height: '100%',
            width: windowWidth - 40,
            flex: 1,
          }}>
          <Card
            source={IMAGES.Vector}
            title={'Allow Moniee to read Contacts?'}
            subTitle={'To request or send funds, Moniee requires your '}
            subTitle2={'permission to read your contacts.'}
            btnLabel={'Grant Access'}
            onPress={() => processUserAcceptAccess()}
          />
        </View>
      </Modal>
      <Modal
        isVisible={showContactModal}
        onClose={() => setShowContactModal(!showContactModal)}>
        <View
          style={{
            marginTop: 40,
            height: '100%',
            width: windowWidth - 40,
            flex: 1,
          }}>
          <SelectContactHolder>
            <NameHolder>
              <NameText>Jason Derulo</NameText>
              <ContactCloseImage source={IMAGES.CloseOutline} />
            </NameHolder>
          </SelectContactHolder>

          <SelectContactHolder2 style={{ borderBpttom: 1 }}>
            <ImageHolder>
              <ContactImage source={IMAGES.Img1} />
              <ContactCloseImage2 source={IMAGES.CloseFill} />
            </ImageHolder>
          </SelectContactHolder2>

          <Hr />
          {UserContactList.map(item => (
            <ContactList
              ImageSource={item.ImageSource}
              contactName={item.contactName}
              miniDetail={item.miniDetail}
              activate={item.activate}
            />
          ))}
        </View>
      </Modal>
    </Wrapper>
  );
};
